from django.shortcuts import render

def index(request):
    return render(request, 'portfolio/home.html', {})

def blogs(request):
    return render(request, 'portfolio/blog.html', {})

def post(request):
    return render(request, 'portfolio/post.html', {})