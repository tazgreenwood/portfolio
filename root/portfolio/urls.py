from django.urls import path

from . import views

app_name = 'portolio'
urlpatterns = [
    path('', views.index, name='index'),
    path('blog/', views.blogs, name='blog'),
    path('post/', views.post, name='post'),
]
